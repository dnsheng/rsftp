from setuptools import setup, find_packages

import rsftp.constants as constants

setup(
    # Application name:
    name="rsftp",
    # Version number
    version=constants.VERSION,
    description="Replicate a remote directory locally with SFTP links",
    # Packages
    packages=find_packages(),

    # Application author details:
    author="Dan Sheng",
    author_email="dnsheng@gmail.com",
    url="https://gitlab.com/dnsheng/rsftp",

    # Include additional files into the package
    include_package_data=True,

    license="LICENSE",
    long_description=open("README.md").read(),

    # Dependent packages (distributions)
    install_requires=[
        "paramiko",
    ],

    entry_points={
        'console_scripts': [
            'rsftp=rsftp.__main__:main',
        ],
    },
)
