import os
import re
import subprocess

def process_ls_dir(line):
    dir_name = line[:-1]
    return dir_name, "[d] {}".format(dir_name)

def process_ls_file(line, cur_dir):
    filename = line.split(' ')[-1]
    filepath = os.path.join(cur_dir, filename)
    return "[-] {}".format(filepath)

def process_ls(stdout):
    cur_dir = None
    processed = []
    for line in filter(None, stdout.split('\n')):
        if line[0] == '/':
            cur_dir, line  = process_ls_dir(line)
            processed.append(line)
        elif line[0] == '-':
            processed.append(process_ls_file(line, cur_dir))

    return processed

def build_list(local_dir):
    stdout = subprocess.check_output(['ls', '-lR', local_dir]).decode()
    local = process_ls(stdout)
    return local

items = build_list("/home/dan/rsftp")
for i in sorted(items):
    print(i)
