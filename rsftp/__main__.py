# This file is part of RSFTP.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""This is the main module to start RSFTP"""

import sys
from argparse import ArgumentParser, HelpFormatter

import rsftp.constants as constants
from rsftp.replicator import Replicator


def _init_args():
    """Parses command line arguments for RSFTP"""
    parse = ArgumentParser(prog="rsftp",
                           usage="%(prog)s [OPTIONS] REMOTE_DIR LOCAL_DIR",
                           description="Replicate remote directories and \
                                        files locally as SFTP links",
                           epilog="Author: DNSheng",
                           add_help=False,
                           formatter_class=lambda prog:
                           HelpFormatter(prog, max_help_position=79))
    # ========================================================================
    # Flags
    # ========================================================================
    parse.add_argument("-h", "--help",
                       action="help",
                       help="Print the help menu and exit")

    parse.add_argument("-v", "--version",
                       action="version",
                       version=constants.VERSION,
                       help="Print program version and exit")

    parse.add_argument("-k", "--keep",
                       action="store_true",
                       help="Keep local directories and files not found \
                             remotely")

    parse.add_argument("-p", "--port",
                       type=str,
                       default=constants.DEFAULT_SSH_PORT,
                       help="The custom SSH port to connect to " +
                       f"(default: {constants.DEFAULT_SSH_PORT})")

    parse.add_argument("--ssh-config",
                       type=str,
                       default=constants.SSH_CONFIG_FILE,
                       help="An SSH configuration file to reference " +
                       f"(default: {constants.SSH_CONFIG_FILE})")
    parse.add_argument("--processes",
                       type=int,
                       default=constants.DEFAULT_PROCESSES,
                       help="Number of processes to use when replicating "
                            f"files (default: {constants.DEFAULT_PROCESSES})")

    # =========================================================================
    # Positional args
    # =========================================================================
    parse.add_argument("REMOTE_ADDR",
                       type=str,
                       help="SSH address and path of remote directory \
                             (user@host:/path/to/dir OR \
                             ssh_alias:/path/to/dir)")

    parse.add_argument("LOCAL_DIR",
                       type=str,
                       help="Path of a local directory (/path/to/dir)")

    return parse.parse_args()


def main():
    """Command-line entry point for RSFTP"""
    args = _init_args()

    replicator = Replicator(args)

    try:
        replicator.replicate(args.REMOTE_ADDR, args.LOCAL_DIR)
    except KeyboardInterrupt:
        sys.stdout.write('\nKeyboard interrupt detected -- exiting rsftp\n')
        sys.exit(0)
    except (FileNotFoundError, LookupError, ValueError,
            NotADirectoryError, ConnectionError) as e:
        print(f"{e.__class__.__name__}: {e}")
    finally:
        replicator.close()
