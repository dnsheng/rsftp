# This file is part of RSFTP.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""This module contains the Replicator class"""

import os
import re
import sys
import shutil
import difflib
import warnings
import subprocess
from multiprocessing.dummy import Pool as ThreadPool

import paramiko


class Replicator():
    """Replicate a remote directory locally.

    The Replicator will extract information from local and remote
    directories, find differences, then rebuild the local directory.
    It also handles connecting to the SSH server through paramiko.
    """

    def __init__(self, args):
        self._args = args
        self._client = paramiko.SSHClient()
        self._client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self._ssh_config = self._check_ssh_config()

    def close(self):
        """Cleans up Replicator by closing all connections"""
        self._client.close()

    def _check_ssh_config(self):
        """Finds SSH config and returns paramiko.SSHConfig()

        The SSH config can be given by the user, but if not provided the
        default location/file is used.
        By parsing the SSH config file, RSFTP can accept SSH aliases.
        """
        ssh_config = paramiko.SSHConfig()
        user_conf = self._args.ssh_config
        # Config found will be parsed and saved
        if os.path.exists(user_conf):
            with open(user_conf) as f:
                ssh_config.parse(f)
        elif self._args.ssh_config:
            raise FileNotFoundError("The given ssh config file was not found")

        return ssh_config

    def _get_ssh_host(self, ssh_addr):
        """Return the host and alias of the ssh address

        If there is no alias for the host, then the alias is set to None
        If there is no host for the alias, a ValueError exception is raised
        """

        # Filter out URL addresses
        if '.' in ssh_addr:
            # If there is a URL, there must be a user given too
            host, alias = ssh_addr.split('@')[-1], None
        else:
            # Get the alias and look for the hostname in the config
            alias = ssh_addr.split('@')[-1] if '@' in ssh_addr else ssh_addr
            host = self._ssh_config.lookup(alias)['hostname']
            # If hostname non-existent, then the hostname will be the alias
            if host == alias:
                raise ValueError("No hostname could be found for the " +
                                 f"SSH host: '{alias}'")
        return host, alias

    def _get_ssh_user(self, ssh_addr, alias):
        """Return the user of the ssh address"""
        # '@' exists, thus the username is given
        if '@' in ssh_addr:
            user = ssh_addr.split('@')[0]
        elif '.' in ssh_addr:
            # An address is given but no '@' was found, thus no username given
            raise ValueError("No username was provided for the SSH address: " +
                             f"'{ssh_addr}'")
        else:
            # A lookup in the config with the alias is necessary
            config = self._ssh_config.lookup(alias)
            # Check safely, as a username may not be defined
            if 'user' in config.keys():
                user = config['user']
            else:
                raise LookupError("No username could be found for the " +
                                  f"SSH host: '{alias}'")
        return user

    def _get_ssh_info(self, remote_addr):
        """Return the SSH user, address, and port"""
        # Split SSH address from directory, join because there may be a port #
        ssh_addr = ':'.join(remote_addr.split(':')[:-1])
        port = self._args.port
        host, alias = self._get_ssh_host(ssh_addr)
        user = self._get_ssh_user(ssh_addr, alias)

        return user, host, port

    @staticmethod
    def _is_valid_local_dir(directory):
        """Returns True if a directory exists locally"""
        return os.path.exists(directory) and os.path.isdir(directory)

    def _get_remote_dir(self, remote_addr):
        """Returns the remote directory from the SSH address"""
        # Some directories may have a colon so a join is needed
        dir_ = ":".join(remote_addr.split(':')[1:])
        # Get full directory path
        return self._full_remote_dir(dir_)

    def _is_valid_remote_dir(self, dir_):
        """Returns True if a directory exists remotely"""
        # Construct a remote command to check for dir existence
        remote_cmd = f"[ -d `readlink -f {dir_}` ] && echo 0 || echo 1"
        # Run and parse output from stdout
        _, stdout, _ = self._client.exec_command(remote_cmd)
        stdout_lines = stdout.readlines()
        status = stdout_lines[-1][0]

        return status == "0"

    def _init_local(self, local_dir):
        """Prepares the local directory for replication.

        Currently, it checks if local folder exists and if so, returns the
        full path
        """
        local_dir = self._full_local_dir(local_dir)
        if not self._is_valid_local_dir(local_dir):
            raise NotADirectoryError("Error: Local directory either " +
                                     "non-existent or not a directory")
        return local_dir

    def _init_remote(self, remote_addr):
        """Prepares the remote directory for replication.

        First, the remote address must be parsed for information regarding
        the SSH server and directory. Then, an SSH connection is attempted.
        If successful, then the existence of the directory is checked.

        Returns the SFTP address for the host, and path of the remote
        directory parsed from the remote address
        """
        # Parse remote address
        ssh_user, ssh_addr, ssh_port = self._get_ssh_info(remote_addr)

        # Attempt SSH connection
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            try:
                self._client.connect(ssh_addr, username=ssh_user,
                                     port=ssh_port)
            except Exception:
                raise ConnectionError("Error: Could not connect to " +
                                      "the SSH address")

        # Expand and validate remote folder
        remote_dir = self._get_remote_dir(remote_addr)
        if not self._is_valid_remote_dir(remote_dir):
            raise NotADirectoryError("Error: Remote directory either " +
                                     "non-existent or not a directory")

        # Build the SFTP address for the directory
        remote_host = remote_addr.split(':')[0]
        sftp_addr = f"sftp://{remote_host}{remote_dir}"

        return sftp_addr, remote_dir

    @staticmethod
    def _process_tree_line(line, dir_name):
        """Returnes a processed line from the output of the tree command.

        This can be used for both remote and local tree output.
        """
        # Remove file permissions, leaving directory flag
        line = re.sub(r"(?<=\[(d|-))[rwx-]{9}(?=\])", "", line)
        # Remove parent directory name from path
        line = re.sub(r"(?<=^\[[d-]\]\s{2})" + dir_name, ".", line)
        # Remove carriage return
        line = re.sub(r"\r", "", line)
        # Remove sftp extension
        line = re.sub(r"\.sftp$", "", line)
        # Remove trailing/leading whitespace
        line = line.strip()

        return line

    def _full_remote_dir(self, remote_dir):
        """Returns the full path of the remote directory.

        Because the user may enter a directory with symlinks and '~',
        this method is necessary to ensure the contents of the SFTP
        links work.
        """
        # Use readlink to get the full path
        readlink_cmd = f'readlink -f {remote_dir}'
        _, stdout, _ = self._client.exec_command(readlink_cmd, get_pty=True)
        # Process STDOUT
        raw_output = [line for line in iter(stdout.readline, "")]
        full_dir = raw_output[0][:-2]
        return full_dir

    @staticmethod
    def _full_local_dir(local_dir):
        """Returns the full path of the remote directory.

        This is needed because we want to be explicit when we run the
        tree command.
        """
        # First expand the '~'
        path = os.path.expanduser(local_dir)
        # Expand variables
        path = os.path.expandvars(path)
        # Deal with symlinks and get full path
        path = os.path.abspath(path)

        return path

    def _build_remote_tree(self, remote_dir):
        """Returns the remote tree file structure

        The tree command is executed on the SSH server and the STDOUT
        is examined. For each line read from STDOUT, it is formatted
        and appended to a list. At the end, the list is returned.

        This is done instead of generating the file on the SSH server
        and downloading it using SFTP. When that was attempted, the
        remote tree file was not completely downloaded and thus missing
        information.

        Furthermore, this allows the program to not have to create new
        files. With the directory data in variables, manipulation and
        processing is easier.

        The remote directory should be explicit and have no symlinks or
        relative paths.
        """
        sys.stdout.write("Generating remote directory structure...")
        # Grab STDOUT from running tree
        try:
            tree_cmd = f'tree -ifp {remote_dir}'
            _, stdout, _ = self._client.exec_command(tree_cmd, get_pty=True)
            # Process output per line and store to list
            r_tree = [self._process_tree_line(line, remote_dir)
                      for line in iter(stdout.readline, "")]
        except Exception:
            sys.stdout.write("Failed\n")
            raise OSError("'tree' program not found on remote system")
        sys.stdout.write("Done\n")
        return r_tree

    def _build_local_tree(self, local_dir):
        """Returns the local tree file structure

        The local directory should be explicit and have no symlinks or
        relative paths.
        The values are generated from STDOUT returned by a subprocess call.
        """
        sys.stdout.write("Generating local directory structure...")
        # Grab STDOUT from running tree
        stdout = subprocess.check_output(["tree", "-ifp",
                                          local_dir]).decode()
        # Process output per line and store to list
        l_tree = [self._process_tree_line(line, local_dir)
                  for line in stdout.split('\n')]
        sys.stdout.write("Done\n")
        return l_tree

    def get_diff(self, local, remote):
        """Returns a list of additions and list of substractions"""
        # Note that raw_diff is a generator
        raw_diff = difflib.unified_diff(local, remote, lineterm='', n=0)
        diff = self._refine_diff(raw_diff)
        add_files, add_dirs, deletions = self._seperate_diffs(diff)
        if add_dirs:
            add_dirs = self._refine_dirs(add_dirs)

        return add_files, add_dirs, deletions

    def _seperate_diffs(self, diffs):
        """Returns a list of additions and a list of deletions

        Given the list of diffs, the first character of each line is examined.
        A "+" sign means that the file only exists remotely and thus should be
        added locally.
        A "-" sign means that the file only exists locally and thus should be
        deleted.
        """
        add_files, add_dirs, deletions = [], [], []
        for line in diffs:
            if line[0] == "+":
                if self._is_directory_diff(line):
                    add_dirs.append(line)
                else:
                    add_files.append(line)
            else:
                deletions.append(f"{line}.sftp")
        return add_files, add_dirs, deletions

    @staticmethod
    def _refine_dirs(dirs):
        """Return a list of all leaf directories from the directory tree

        Note that the list of dirs given is sorted by name alphabetically
        """
        # Start with the first directory in the list
        refined = []
        prev_dir = f"{dirs[0]}/"
        refined.append(dirs[0])
        # Loop across remaining directories
        for dir_ in dirs[1:]:
            # If there is a child dir next, remove the current dir
            if prev_dir in dir_:
                refined.pop()
            # Add the directory to the list
            refined.append(dir_)
            # Remember the name for the next dir
            prev_dir = f"{dir_}/"

        return refined

    @staticmethod
    def _refine_diff(raw_diff):
        """Returns a sorted and refined diff

        Lines used in diff that don't contain files should be removed.
        Ex.
            @@@ L23, R25 @@@

        In some cases, the raw diff shows files that exist both locally and
        remotely yet have the exact same path relative to the parent folder.
        Eg.
            -[-]  /path/to/file1.txt
            ...
            +[-]  /path/to/file1.txt
        In such a case, there is no change to be made so both entries should
        be removed.
        """
        reduced_diff = []
        for line in raw_diff:
            # Filter only relevant lines from raw_diff
            if line[0:2] in ("+[", "-["):
                # Build an opposing file diff
                diff_type = "-" if line[0] == "+" else "+"
                opposing = diff_type + line[1:]
                # Ensure no two diffs with same path but different signs
                if opposing not in reduced_diff:
                    reduced_diff.append(line)
                else:
                    reduced_diff.remove(opposing)
        return reduced_diff

    def replicate(self, remote_addr, local_dir):
        """The main method called to replicate a directory locally"""
        # Init connection and validate for both remote and local locations
        local_dir = self._init_local(local_dir)
        sftp_addr, remote_dir = self._init_remote(remote_addr)

        # Get remote and local directory information
        l_tree = self._build_local_tree(local_dir)
        r_tree = self._build_remote_tree(remote_dir)

        # Compare the two directories, getting additions and deletions
        add_files, add_dirs, deletions = self.get_diff(l_tree, r_tree)

        # Return if no changes to be made
        additions = add_files or add_dirs
        update_needed = additions or (deletions and not self._args.keep)
        if not update_needed:
            sys.stdout.write("No changes to be made. Exiting.\n")
            return

        sys.stdout.write("Replicating files...")
        # Go through the differences and replicate
        static_info = (local_dir, sftp_addr)
        self.duplicate(add_files, add_dirs, deletions, static_info)
        sys.stdout.write("Done\n")

    def duplicate(self, add_files, add_dirs, dels, static_info):
        """Create processes to add and delete files/dirs."""
        # Unpack info tuple
        local_dir, sftp_addr = static_info
        # Create the addition process and start
        a_dirs = [(d, local_dir) for d in add_dirs]
        a_files = [(f, local_dir, sftp_addr) for f in add_files]

        # Create a pool of workers (processes)
        pool = ThreadPool(self._args.processes)

        # Start with directories, then files
        pool.starmap(self.add_dir, a_dirs)
        pool.starmap(self.add_file, a_files)

        # Skip deletion if flag toggled
        if not self._args.keep:
            d_files = [(d, local_dir) for d in dels]
            pool.starmap(self.delete_file, d_files)

        # Clean pool
        pool.close()
        pool.join()

    @staticmethod
    def add_dir(addition, local_dir):
        """Create a specified directory locally."""
        directory_name = '/'.join(addition.split('/')[1:])
        local_directory = os.path.join(local_dir, directory_name)
        os.makedirs(local_directory)

    @staticmethod
    def add_file(addition, local_dir, sftp_addr):
        """Create a specified SFTP link file locally."""
        # Create a file
        sftp_file = ' '.join(addition.split(' ')[2:])[2:]
        local_base = os.path.join(local_dir, sftp_file)
        local_file = f"{local_base}.sftp"
        # Escape round brackets, period, and space
        remote_file = re.sub(r"([\ \(\)\.\'])", r'\\\1', sftp_file)
        content = os.path.join(sftp_addr, remote_file)
        with open(local_file, 'w') as f:
            f.write(content)

    def delete_file(self, deletion, local_dir):
        """Delete a specified local directories/SFTP link files"""
        if self._is_directory_diff(deletion):
            directory_name = '/'.join(deletion.split('/')[1:])
            local_directory = os.path.join(local_dir, directory_name)
            # Multithreading may disallow
            try:
                shutil.rmtree(local_directory)
            except FileNotFoundError:
                pass
        else:
            # Delete a file
            sftp_file = ' '.join(deletion.split(' ')[2:])[2:]
            local_file = os.path.join(local_dir, sftp_file)
            os.remove(local_file)

    @staticmethod
    def _is_directory_diff(line):
        return line[2] == "d"
