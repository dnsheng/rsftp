# This file is part of RSFTP.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests the Replicator class in RSFTP"""
import os
import sys
import unittest
from collections import namedtuple

sys.path.append(os.path.abspath('..'))

from rsftp.replicator import Replicator


class ReplicatorTest(unittest.TestCase):
    """Tests the Replicator class in RSFTP"""

    @classmethod
    def setUp(cls):
        Arg = namedtuple('Arg', 'port ssh_config')
        mock_arg = Arg("22", "~/.ssh/config")
        cls.rep = Replicator(mock_arg)

    def test_close(self):
        """Test if ssh connection is truly terminated"""
        self.rep.close()
        self.assertRaises(AttributeError, self.rep._client.exec_command, "ls")

    def test_get_ssh_host(self):
        """Test if the SSH host address is extracted properly"""
        # ssh_addr, host, exception
        test_cases = [("user@somewebsite.com", "somewebsite.com", None),
                      ("user@home", "ayashii.info", None),
                      ("home", "ayashii.info", None),
                      ("user@notasetalias", None, ValueError),]

        for ssh_addr, expected, exception in test_cases:
            if exception:
                self.assertRaises(exception, self.rep._get_ssh_host, ssh_addr)
            else:
                actual, _ = self.rep._get_ssh_host(ssh_addr)
                self.assertEqual(actual, expected)

    def test_get_ssh_user(self):
        """Test if the SSH username is extracted properly"""
        # ssh_addr, alias, expected, exception
        test_cases = [("dan@somewebsite.com", None, "dan", None),
                      ("somewebsite.com", None, None, ValueError),
                      ("home", "home", "pi", None),
                      ("dan@home", "home", "dan", None),
                      ("nouser", "nouser", None, LookupError),]

        for ssh_addr, alias, expected, exception in test_cases:
            if exception:
                self.assertRaises(exception, self.rep._get_ssh_user, ssh_addr,
                                  alias)
            else:
                actual = self.rep._get_ssh_user(ssh_addr, alias)
                self.assertEqual(actual, expected)
