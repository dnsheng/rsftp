# RSFTP - SFTP file replicator
### Description:

Replicate remote directories locally with SFTP links.


RSFTP given a remote SFTP directory will replicate the directory structure to a local
destination. All files replicated will be SFTP links (.sftp extension). The content of each
file is the SFTP address of the file.


These files may be read for quick download or streaming with an external program (eg. MPV).


RSFTP uses an SSH connection and will read the default .ssh configuration file located on
your system. As a result, SSH aliases and keys are supported for shorter command-line
invokations of RSFTP.

### Requirements:

+ python 3.6+
+ paramiko

### Testing:

Go to the folder containing setup.py and run the following command:

```
python -m unittest
```

### Installation:

Go to the folder containing setup.py and run the following command:

```
python setup.py install
```

### Usage:

Get help by running:

```
rsftp -h
```

Sample usage:

```
rsftp user@host:~/path/to/dir path/to/local/dest
```

### TODO:

```
+ Nicer output
+ Replace the 'tree' command with python-native method (os.listdir)
	+ Requires python installed on remote system
	+ Maybe parse 'ls -lR'
+ Check for comparison logic -- are we actually comparing SFTP files?
	+ If the actual (.mp4, .mkv, .jpg, etc.) file exists, do we count it as existing?
	+ Perhaps keep that feature
	+ Maybe add size comparison too
+ Unit tests
```
